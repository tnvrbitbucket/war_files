package com.samriton.app.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.samriton.app.service.MailService;

@Controller
public class AppController {

	@Autowired
	private MailService mailService;
	@GetMapping("/")
	public String getHomePage() {
		return "welcome";
	}

	@GetMapping("/index.html")
	public String getIndexPage() {
		return "welcome";
	}

	@GetMapping("/about.html")
	public String getAbout() {
		return "about";
	}

	@GetMapping("/single-blog.html")
	public String getSingleBlog() {
		return "single-blog";
	}

	@GetMapping("/impact.html")
	public String getImpact() {
		return "impact";
	}

	@GetMapping("/element.html")
	public String getElement() {
		return "element";
	}

	@GetMapping("/contact.html")
	public String getContact() {
		return "contact";
	}

	@GetMapping("/causes.html")
	public String getCauses() {
		return "causes";
	}

	@GetMapping("/blog.html")
	public String getBlog() {
		return "blog";
	}

	@GetMapping("/causes-details.html")
	public String getCausesDetails() {
		return "causes-details";
	}

	@PostMapping("/sent_email")
	public String sentEmail(@RequestParam("email") String email) {
		System.out.println(email);
		mailService.sentEmail(email);
		return "mail";
	}
}
