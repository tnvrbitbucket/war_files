package com.samriton.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReadFoundationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadFoundationApplication.class, args);
	}

}
