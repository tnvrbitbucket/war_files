package com.samriton.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

	@Autowired
	private JavaMailSender javaMailSender;

	@Override
	public void sentEmail(String userName) {
		SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(userName);

        msg.setSubject("Welcome To Read Foundation");
        msg.setText("You Subscribed For News. Thanks");
        try {
        javaMailSender.send(msg);
        }catch(Exception exception) {
        	System.out.println("exception:"+ exception.getMessage());
        }
	}
	
}
